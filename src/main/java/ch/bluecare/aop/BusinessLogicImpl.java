package ch.bluecare.aop;

@Log
public class BusinessLogicImpl implements BusinessLogic{

  @Override
  @Log
  public Result go(final int amount) {
    return new Result(amount);
  }
}
