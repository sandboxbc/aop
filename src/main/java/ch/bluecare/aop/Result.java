package ch.bluecare.aop;

public class Result {
private final int amount;

  public Result(final int amount) {
    this.amount = amount;
  }

  public int getAmount() {
    return amount;
  }

  public String toString(){
    return "Resulting amount " + amount;
  }
}
