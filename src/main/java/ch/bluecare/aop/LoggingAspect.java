package ch.bluecare.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

@Aspect
public class LoggingAspect {

  @Around("@annotation(ch.bluecare.aop.Log)")
  public Object log (ProceedingJoinPoint thisJoinPoint) throws Throwable {
    final String methodName = thisJoinPoint.getSignature().getName();
    Object[] methodArgs = thisJoinPoint.getArgs();
    System.out.println("Call method " + methodName + " with arg " + methodArgs[0]);
    Object result = thisJoinPoint.proceed();
    System.out.println("Method " + methodName + " returns " + result);
    return result;
  }
}
