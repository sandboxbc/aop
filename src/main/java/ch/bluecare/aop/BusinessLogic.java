package ch.bluecare.aop;

public interface BusinessLogic {
    Result go(int amount);
}
