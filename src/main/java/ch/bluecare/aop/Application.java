package ch.bluecare.aop;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Application {

  public static void main(String[] args) {
    ClassPathXmlApplicationContext context =
        new ClassPathXmlApplicationContext("aop.xml");

    final BusinessLogic businessLogic = (BusinessLogic)context.getBean("businessLogic");
    System.out.println(businessLogic.go(42));
    context.close();
  }
}
